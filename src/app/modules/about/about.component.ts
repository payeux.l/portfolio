import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ transform: 'translateY(0)' })),
      transition('void => *', [
        style({ transform: 'translateY(100%)' }),
        animate(200)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'translateY(0)' }))
      ])
    ]),
    trigger('myInsertRemoveTrigger', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('1000ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('100ms', style({ opacity: 0 }))
      ])
    ]),
  ]
})
export class AboutComponent implements OnInit {

  skills=true;
  experiences=false;
  educations=false;

  constructor() { }

  ngOnInit(): void {
  }

  experienceClick() {
    this.experiences = true;
    this.skills = false;
    this.educations = false;
  }

  skillsClick() {
    this.skills = true;
    this.experiences = false;
    this.educations = false;
  }
  educationsClick() {
    this.skills = false;
    this.experiences = false;
    this.educations = true;
  }

  

}
